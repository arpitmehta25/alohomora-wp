<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'alhm_wpdb' );

/** MySQL database username */
define( 'DB_USER', 'alhm_wpdbuser' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Alhm123@' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3_APM?/w]fMC3uEN2vuU%1oZk{U$LTZzF,XWH60whm|n|iDXdU>D_WGE;(`{8Bu|' );
define( 'SECURE_AUTH_KEY',  'jp:cZug)0+[?Q0fKLs:17^Yy^,E*QhJ*J~s/{iJV[}sg}0,:ICK:eBKFW2wZ6O@_' );
define( 'LOGGED_IN_KEY',    'o?J5t1=I]N5IJ:2hSi?Yxs&:+vt+t#Py/S{[!A`jnw>nwu ^)^3FFgV;(c;z<_y{' );
define( 'NONCE_KEY',        '%MW#VyUR8}<ed%}.l5[@4uXC&*r}al<3k33[f@S&+pv2&,iJDeWu5LNG |>S))sH' );
define( 'AUTH_SALT',        '#&[#1CQ4]<m# qoKGFkL+UQ=+.Ur(I1o#UAkx:B{_mm6KEF__OW<(Qzy<!:vBp>J' );
define( 'SECURE_AUTH_SALT', '}:J!ZGo&r-*oujv,2O:M?hd+s+.Ar0};|OS[hpXiFM?1gK]x~mH-h){WOrQZyD=t' );
define( 'LOGGED_IN_SALT',   'F^h_jn?xHJ=KZ%F0KJjB@rG8ePj#wWe)|S4&hz&}q};-+:N@2zi$<xcm22La6Xi)' );
define( 'NONCE_SALT',       '}fHb7|828}WIRaj ,vf}B0of54Yd.@,gq9{aFie8Glpk>wXIPJK~T<`~#e!T4v3>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
